import os, sys, socket



sys.path.append('/home/<username>')
sys.path.append('/home/<username>/trendyou')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "trendyou.settings")

import djcelery
djcelery.setup_loader()


# This application object is used by the development server
# as well as any WSGI server configured to use this file.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

