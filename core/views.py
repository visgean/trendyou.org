from django.views.generic.simple import direct_to_template
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect 
from django.core.urlresolvers import reverse

@login_required
def overview(request):
	return redirect(reverse("facebook_overview"))
	#return direct_to_template(request, "core/overview.html", {})