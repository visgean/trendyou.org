from django.db import models
from django.contrib.auth.models import User
from social_auth.models import UserSocialAuth

from trendyou import fbook

from trendyou.fbook.tools import GraphAPI 

class UserProfile(models.Model):
	user = models.ForeignKey(User)
	pgp_key = models.TextField()
	
	def __unicode__(self):
		return self.user.username
	
	@property
	def facebook_profile(self):
		#return UserSocialAuth.objects.filter(user=self.user, provider="facebook")[0]
		return self.user.social_auth.all()[0] if len(self.user.social_auth.all()) else None
	
	@property
	def graph_api_key(self):
		return self.facebook_profile.tokens["access_token"]
	
	@property
	def graph_api(self):
		return GraphAPI(self.graph_api_key, timeout=1)