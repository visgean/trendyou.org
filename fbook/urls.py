from django.conf.urls.defaults import *



urlpatterns = patterns("trendyou.fbook.views",
		url(r"^$", view="overview", name="facebook_overview"),
		url(r"^threads$", view="threads", name="thread_list"),
		url(r"^thread/(?P<uid>\w+)$", view="thread", name="thread_read"),
					)
