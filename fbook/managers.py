from django.db import models
from django.db.models import Q


class ThreadManager(models.Manager):
	def for_user(self, profile):
		query = super(ThreadManager, self).get_query_set()
		
		return query.filter(sync__profile=profile) 

