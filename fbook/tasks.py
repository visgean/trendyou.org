from celery import task
from datetime import datetime
from facebook import GraphAPIError

from trendyou.fbook.models import FacebookMessage, FacebookThread


MESSAGES_PER_FQL = 30


def process_messages(messages):
    for message in messages:
        FacebookMessage.objects.get_or_create(thread=FacebookThread.objects.get(thread_id=message["thread_id"]),
                                              created_time=datetime.fromtimestamp(message["created_time"]),
                                              author_id=message["author_id"],
                                              text=message["body"]
                                              )


def close_thread(thread):
    print "Thread synced with %s" % (len(thread.facebookmessage_set.all()))
    thread.synced = True
    thread.save()


@task(default_retry_delay=10 * 60, max_retries=10)
def download_table(api, table_name, fields, proccess_data, where_clause, limit_min=0, limit_max=30, post_hook=None, post_hook_args=(), objects_per_fql=30):
    if not api.is_blocked(table_name):
        query = "SELECT %s FROM %s WHERE %s=%s LIMIT %s" % (
            ", ".join(fields), table_name, where_clause[0], where_clause[1],
            "%s, %s" % (limit_min, limit_max) if limit_max else "%s" % (limit_min)
        )

        try:
            records = api.fql(query)

            proccess_data(records)

            if len(records) < objects_per_fql:
                if post_hook:
                    post_hook(*post_hook_args)
            else:
                download_table.apply_async(kwargs={
                    "api": api,
                    "table_name": table_name,
                    "fields": fields,
                    "proccess_data": proccess_data,
                    "where_clause": where_clause,
                    "post_hook": close_thread,
                    "post_hook_args": post_hook_args,
                    "objects_per_fql": objects_per_fql,
                    "limit_min": limit_min + objects_per_fql,
                    "limit_max": limit_max + objects_per_fql,
                })

        except GraphAPIError:
            print "blocking api for " + table_name
            api.set_blocked(table_name)
            download_table.retry()

    else:
        download_table.retry()  # throw=False)


@task
def populate_sync(sync):
    threads = sync.profile.graph_api.fql("SELECT thread_id, message_count,recipients FROM thread WHERE folder_id=0")
    for thread in threads:
        thrd = FacebookThread(sync=sync, thread_id=thread["thread_id"], recipients=str(thread["recipients"]))
        thrd.save()

        download_table.apply_async(kwargs={
            "api": sync.profile.graph_api,
            "table_name": "message",
            "fields": ("author_id", "message_id", "body", "created_time", "thread_id"),
            "proccess_data": process_messages,
            "where_clause": ("thread_id", thread["thread_id"]),
            "post_hook": close_thread,
            "post_hook_args": (thrd,),
            "objects_per_fql": 30,
        })
