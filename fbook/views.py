from django.views.generic.simple import direct_to_template
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from trendyou.fbook.models import Sync, FacebookThread, FacebookMessage
from trendyou.fbook.tasks import populate_sync
from trendyou.core.models import UserProfile

@login_required
def overview(request):	
	profile, created = UserProfile.objects.get_or_create(user=request.user)
	sync, created = Sync.objects.get_or_create(profile=profile)
	if created:
		populate_sync(sync)
	
	if not sync.synced:
		return direct_to_template(request, "facebook/loading.html", {"sync" : sync, })
	else:
		
		return direct_to_template(request, "facebook/overview.html", {})
	

@login_required
def threads(request):
	profile = request.user.get_profile()
	threads = FacebookThread.objects.for_user(profile)	
	
	return direct_to_template(request, "facebook/read/threads.html", {"threads": threads})


@login_required
def thread(request, uid):
	profile = request.user.get_profile()
	thread = FacebookThread.objects.for_user(profile).get(id=uid)	
	
	paginator = Paginator(FacebookMessage.objects.filter(thread=thread), 50, 10)
	
	page = request.GET.get('page')
	try:
		messages = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		messages = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		messages = paginator.page(paginator.num_pages)
	
	return direct_to_template(request, "facebook/read/thread.html", 
							{
							"thread": thread,
							"fmessages" : messages,
							"paginator" : paginator,
							})