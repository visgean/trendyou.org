#! /usr/bin/python
# -*- coding: UTF-8 -*-

import facebook
import urllib2
import time

from trendyou.core.rds import r




class GraphAPI(facebook.GraphAPI):	
	def fql(self, query, set_cache=False, from_cache=False, refresh_time=24*60*60*7):
		def raw_fql():
			"Really, get fql!"
			count = 0
			try:
				return super(GraphAPI, self).fql(query)
			except urllib2.URLError:
				if count < 9:
					count += 1
					return self.fql(query)
				
		if from_cache and r.hexists("fql_cache", query) and (time.time() - float(r.hget("fql_cache_time", query))) < refresh_time:
			return eval(r.hget("fql_cache", query))
		else:
			result = raw_fql()
			if set_cache:
				r.hset("fql_cache_time", query, time.time())
				r.hset("fql_cache", query, result)
			return result
			
		
		
	def get_object(self, uid, set_cache=True, from_cache=True, refresh_time=24*60*60*7):
		def raw_get():
			"this func actually gets the object"
			count = 0
			try:
				return super(GraphAPI, self).get_object(uid)
			except urllib2.URLError:
				if count < 9:
					count += 1
					return self.get_object(uid)
				return None
		
		if from_cache and r.hexists("graph_cache", uid) and (time.time() - float(r.hget("graph_cache_time", uid))) < refresh_time:
			result = eval(r.hget("graph_cache", uid)) 	# I mean, i should use json... but why? the eval does the thing and it must have been tested since it has been saved as dict in string!			
		else:
			result = raw_get()
			if set_cache:
				r.hset("graph_cache_time", uid, time.time())
				r.hset("graph_cache", uid, result)
				
		return result			
			
			

	def set_blocked(self, table_name, expires=10 * 60):
		"""
		Sets that this object exceeded limit of queries per second 
		and sets the timestamp when it should be ok to start using it again.
		"""

		return r.hset("blocked_keys_" + self.access_token, table_name, time.time() + expires)

	def is_blocked(self, table_name):
		"Tests if this api key exceeded limit of queries per second (actually to any table!)"
		
		return float(r.hget("blocked_keys_" + self.access_token, table_name)) > time.time() if r.exists("blocked_keys_" + self.access_token) and r.hexists("blocked_keys_" + self.access_token, table_name) else False








