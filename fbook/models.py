import json

from django.db import models

from trendyou.core.models import UserProfile
from trendyou.fbook.managers import ThreadManager
from trendyou.settings import location


class Sync(models.Model):
    "This class holds syncing information - when was the last syncing with facebook"

    profile = models.ForeignKey(UserProfile)
    sync_time = models.DateTimeField(auto_now_add=True)

    @property
    def percentage(self):
        if len(FacebookThread.objects.filter(sync=self)):
            return float(len(self.facebookthread_set.filter(synced=True))) / len(self.facebookthread_set.all()) * 100
        else:
            return 0

    @property
    def synced(self):
        return self.percentage == 100

    def __unicode__(self):
        return "%s, %s" % (self.profile, self.sync_time)


class FacebookUser(models.Model):
    profile = models.ForeignKey(UserProfile, blank=True)

    fid = models.CharField(max_length=30)
    login = models.CharField(max_length=30)

    first_name = models.CharField(max_length=30, blank=True)
    last_name = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField()

    photo = models.ImageField(upload_to=location + "/photos", blank=True)

    friends = models.ManyToManyField("self")


class FacebookThread(models.Model):
    "Facebook thread"

    thread_id = models.CharField(max_length=30)
    recipients = models.ManyToManyRel(FacebookUser)

    sync = models.ForeignKey(Sync)
    synced = models.BooleanField()

    objects = ThreadManager()

    @property
    def count(self):
        return len(self.facebookmessage_set.all())

#       @property
#       def participants(self):
#               return [self.sync.profile.graph_api.get_object(str(p)) for p in json.loads(self.recipients)]

    @property
    def latest(self):
        return self.facebookmessage_set.latest("created_time")

    def __unicode__(self):
        return self.recipients


class FacebookMessage(models.Model):
    "Facebook message"

    thread = models.ForeignKey(FacebookThread)
    text = models.TextField()
    created_time = models.DateTimeField()
    author_id = models.BigIntegerField()

    def __unicode__(self):
        return self.text

    @property
    def author(self):
        return self.thread.sync.profile.graph_api.get_object(str(self.author_id))

    @property
    def date(self):
        return self.created_time.strftime("%H:%M")
