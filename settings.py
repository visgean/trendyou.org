# Django settings for trendyou project.

import socket

from trendyou.credentials import *


DEBUG = False
TEMPLATE_DEBUG = DEBUG

if socket.gethostname() == "rewitaqia":
	from trendyou.custom_settings.rewitaqia import *
elif socket.gethostname() == "regulus":
	from trendyou.custom_settings.regulus import *
else: # feel free to set here anything you want
	from trendyou.custom_settings.other import *


ADMINS = (
     ('Visgean Skeloru', 'Visgean@gmail.com'),
)

MANAGERS = ADMINS

AUTH_PROFILE_MODULE = 'core.UserProfile'


TIME_ZONE = 'Europe/Prague'

LANGUAGE_CODE = 'cz-cs'

SITE_ID = 1

USE_I18N = True

USE_L10N = True
STATIC_URL = '/static/'
MEDIA_ROOT = location + "media/"
MEDIA_URL = '/media/'

ADMIN_MEDIA_PREFIX = '/static/admin/'


STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'uUQt8UqcCqCiv3dAx1lTWmIk4lCamjFjfZWniq1QsFJdZNWs'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    
	'debug_toolbar.middleware.DebugToolbarMiddleware',

)

INTERNAL_IPS = ('127.0.0.1',)

ROOT_URLCONF = 'trendyou.urls'


INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.admindocs',
    
    "social_auth",
	"debug_toolbar",
	'debug_toolbar_user_panel', 			# http://code.playfire.com/django-debug-toolbar-user-panel/
	"djcelery", 
	"south",

    'trendyou.core',
    'trendyou.fbook',
)   

DEBUG_TOOLBAR_PANELS = (
	"debug_toolbar_user_panel.panels.UserPanel",
    'debug_toolbar.panels.version.VersionDebugPanel',
    'debug_toolbar.panels.timer.TimerDebugPanel',
    'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
    'debug_toolbar.panels.headers.HeaderDebugPanel',
    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
    'debug_toolbar.panels.template.TemplateDebugPanel',
    'debug_toolbar.panels.sql.SQLDebugPanel',
    'debug_toolbar.panels.signals.SignalDebugPanel',
    'debug_toolbar.panels.logger.LoggingPanel',
)


TEMPLATE_CONTEXT_PROCESSORS = (
	"django.contrib.auth.context_processors.auth",
	"django.core.context_processors.debug",
	"django.core.context_processors.i18n",
	"django.core.context_processors.media",
	"django.core.context_processors.static",
	"django.core.context_processors.tz",
	"django.contrib.messages.context_processors.messages",
	
	'social_auth.context_processors.social_auth_by_name_backends',
	'social_auth.context_processors.social_auth_backends',
	'social_auth.context_processors.social_auth_by_type_backends',
)




LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}



AUTHENTICATION_BACKENDS = (
    'social_auth.backends.facebook.FacebookBackend',
	'django.contrib.auth.backends.ModelBackend',
)


DEBUG_TOOLBAR_CONFIG = {
	"INTERCEPT_REDIRECTS" : False
					}

LOGIN_URL          = '/login/'
LOGIN_REDIRECT_URL = '/'

SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/'
#SOCIAL_AUTH_COMPLETE_URL_NAME  = 'socialauth_complete'
#SOCIAL_AUTH_ASSOCIATE_URL_NAME = 'socialauth_associate_complete'


BROKER_URL = 'amqp://guest@localhost:5672//'
#CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'

# celery:
#BROKER_URL = 'amqp://trendyou:QJjbefCzVaq1@localhost:5672/trendyou'
#transport://userid:password@hostname:port/virtual_host


import djcelery
djcelery.setup_loader()

