import getpass 

from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin

from trendyou import settings

admin.autodiscover()

urlpatterns = patterns('',
     url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
     url(r'^admin/', include(admin.site.urls)),
	
	 url(r'', include('debug_toolbar_user_panel.urls')),
     url(r'', include('social_auth.urls')),
     
	 url(r'', include('trendyou.core.urls')),
	 url(r'^facebook/', include('trendyou.fbook.urls')),
	 
	url(r'^login/$', view='django.contrib.auth.views.login', name='auth_login', kwargs={'template_name':'core/login.html'}),
	url(r'^logout/$', view='django.contrib.auth.views.logout', name='auth_logout', kwargs={'next_page':'/', }),
	
	 )



# just a hack for ./manage.py runserver to serve media:
urlpatterns += patterns('',
	(r'^%s(?P<path>.*)$' % settings.MEDIA_URL[1:], 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
)