#! /usr/bin/python
# -*- coding: UTF-8 -*-

import sys, getpass


USER_HOME_FOLDER = '<set name>'
PROJECT_NAME = 'trendyou'
location = "<set location>"

DATABASES = {
	"default" :  {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'django_trendyou', # Or path to database file if using sqlite3.
        'USER': 'trendyou', # Not used with sqlite3.
        'PASSWORD': 'heslo', # Not used with sqlite3.
        'HOST': '127.0.0.1', # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '', # Set to empty string for default. Not used with sqlite3.
        
	    "client_encoding": 'UTF8',
	    "default_transaction_isolation": 'read committed',
	    "timezone": 'UTC'

        
        }
}
	

templates = location + "/templates"
templates_location = templates
MEDIA_ROOT = location + "media/"

STATICFILES_DIRS = (location + "static/",)

TEMPLATE_DIRS = (location + "templates/",)

DEBUG = True

EMAIL_HOST = 'localhost'



