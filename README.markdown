Trendyou.org goals:
==================


* Global:	
 	1. graf poctu radku na obdobi (prijate odeslane)
 	2. pocet statusu
 	3. pocet odkazu
	4. popularita odkazu

* Analyza pratel:
    1. v zavislosti na ucelenosti skupiny (fnetwork map nebo co)
 	2. v zavislosti na intereakci
	3. popularita

* Propojeni s last.fm?

* Analyzovat pratele:
	1. Analyza intereaktivity
	2. graf poctu radku na obdobi (prijate odeslane)
	3. odstartovane versus prijate rozhovory (kdo zacal chatovat)
	4. Pocet napsanejch versus pocet prijatejch lajn
	5. Emotivnost (smajlici, nadavky, vykricniky...)
	6. delka zprav

* Privacy?
	1. Moznost nahrani pgp klice
	2. Odesilani zprav na mail
	3. Kompletni zaloha
	4. kazdy den mazani (defaultne)


* Zkusit upravit code_swarm?


Todo:
-----

1. Zobrazovani zprav: nahoru dat graf poctu zprav
